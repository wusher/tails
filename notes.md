
mix phx.new tails
cs tails/assets
npm install github:tailwindcss/tailwindcss#v2.0.0-beta.3 --save-dev
npm install  postcss-loader --save-dev
npm install  autoprefixer --save-dev

cd js 
npx tailwind init 