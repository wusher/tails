module.exports = {
  theme: {
    fontFamily: {
      display: ['Gilroy', 'sans-serif'],
      body: ['Graphik', 'sans-serif'],
    },
    extend: {
      colors: theme => ({
        primary: {
          normal: "#3c3c3d",
          dark: "#1a1a1a"
        }
      })
    }
  },

  variants: {
    // backgroundColors: ['responsive', 'hover', 'focus'],
  },
  plugins: [
    // Some useful comment
    function ({
      addComponents,
      config
    }) {


      console.log('theme.backgroundColors')
      console.dir(config('theme.backgroundColors'))
      process.exit()


      const buttons = {
        '.btn': {
          padding: '.5rem 1rem',
          borderRadius: '.25rem',
          fontWeight: '600',
        },
        '.btn-primary': {
          backgroundColor: config('theme.colors.primary.normal'),
          color: '#fff',
          '&:hover': {
            backgroundColor: config('theme.colors.primary.dark'),
          },
        },
      }

      addComponents(buttons)
    }
  ]
}